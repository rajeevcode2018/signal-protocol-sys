use std::os::raw as types;
use std::fmt;

use curve;
use ratchet;
use sender;
use session;


/// Address of an AXOLOTL message recipient
#[repr(C)]
#[derive(Debug)]
pub struct Address {
	pub name:      *const u8,
	pub name_len:  usize,
	pub device_id: i32,
}


/// Log levels
#[repr(i32)]
#[derive(Debug, Clone, Copy)]
pub enum LogLevel {
	Error   = 0,
	Warning = 1,
	Notice  = 2,
	Info    = 3,
	Debug   = 4,
}
impl fmt::Display for LogLevel {
	fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
		write!(fmt, "{}", match self {
			&LogLevel::Error   => "Error",
			&LogLevel::Warning => "Warning",
			&LogLevel::Notice  => "Notice",
			&LogLevel::Info    => "Info",
			&LogLevel::Debug   => "Debug",
		})
	}
}


/// Mode settings for the crypto callbacks
#[allow(non_camel_case_types)]
#[repr(i32)]
#[derive(Debug, Clone, Copy)]
pub enum Cipher {
	AES_CTR_NoPadding = 1,
	AES_CBC_PKCS5     = 2,
}


extern "C" {
	pub fn axolotl_type_ref(instance: *mut ::TypeBase);
	pub fn axolotl_type_unref(instance: *mut ::TypeBase);
}


extern "C" {
	/**
	 * Allocate a new buffer to store data of the provided length.
	 *
	 * @param len length of the buffer to allocate
	 * @return pointer to the allocated buffer, or 0 on failure
	 */
	pub fn axolotl_buffer_alloc(len: usize) -> *mut ::Buffer;
	
	/**
	 * Create a new buffer and copy the provided data into it.
	 *
	 * @param data pointer to the start of the data
	 * @param len length of the data
	 * @return pointer to the allocated buffer, or 0 on failure
	 */
	pub fn axolotl_buffer_create(data: *const u8, len: usize) -> *mut ::Buffer;
	
	/**
	 * Create a copy of an existing buffer.
	 *
	 * @param buffer the existing buffer to copy
	 * @return pointer to the updated buffer, or 0 on failure
	 */
	pub fn axolotl_buffer_copy(buffer: *const ::Buffer) -> *mut ::Buffer;
	
	/**
	 * Append the provided data to an existing buffer.
	 * Note: The underlying buffer is only expanded by an amount sufficient
	 * to hold the data being appended. There is no additional reserved space
	 * to reduce the need for memory allocations.
	 *
	 * @param buffer the existing buffer to append to
	 * @param data pointer to the start of the data
	 * @param len length of the data
	 * @return pointer to the updated buffer, or 0 on failure
	 */
	pub fn axolotl_buffer_append(buffer: *mut ::Buffer, data: *const u8, len: usize) -> *mut ::Buffer;
	
	/**
	 * Gets the data pointer for the buffer.
	 * This can be used to read and write data stored in the buffer.
	 *
	 * @param buffer pointer to the buffer instance
	 * @return data pointer
	 */
	pub fn axolotl_buffer_data(buffer: *mut ::Buffer) -> *mut u8;
	
	/**
	 * Gets the length of the data stored within the buffer.
	 *
	 * @param buffer pointer to the buffer instance
	 * @return data length
	 */
	pub fn axolotl_buffer_len(buffer: *mut ::Buffer) -> usize;
	
	/**
	 * Compare two buffers.
	 *
	 * @param buffer1 first buffer to compare
	 * @param buffer2 second buffer to compare
	 * @return 0 if the two buffers are equal, negative or positive otherwise
	 */
	pub fn axolotl_buffer_compare(buffer1: *const ::Buffer, buffer2: *const ::Buffer) -> ::Comparision;

	/**
	 * Free the data buffer.
	 *
	 * @param buffer pointer to the buffer instance to free
	 */
	pub fn axolotl_buffer_free(buffer: *mut ::Buffer);

	/**
	 * Zero and free the data buffer.
	 * This function should be used when the buffer contains sensitive
	 * data, to make sure the memory is cleared before being freed.
	 *
	 * @param buffer pointer to the buffer instance to free
	 */
	pub fn axolotl_buffer_bzero_free(buffer: *mut ::Buffer);

	/**
	 * Allocate a new buffer list.
	 *
	 * @return pointer to the allocated buffer, or 0 on failure
	 */
	pub fn axolotl_buffer_list_alloc() -> *mut ::BufferList;

	/**
	 * Push the provided buffer onto the head of the list.
	 *
	 * @param list the buffer list
	 * @param buffer the buffer to push
	 * @return 0 on success, or negative on failure
	 */
	pub fn axolotl_buffer_list_push(list: *mut ::BufferList, buffer: *mut ::Buffer) -> ::OkStatus;

	/**
	 * Gets the size of the buffer list.
	 *
	 * @param list the buffer list
	 * @return the size of the list
	 */
	pub fn axolotl_buffer_list_size(list: *const ::BufferList) -> types::c_int;

	/**
	 * Free the buffer list, including all the buffers added to it.
	 *
	 * @param list the buffer list
	 */
	pub fn axolotl_buffer_list_free(list: *mut ::BufferList);

	/**
	 * Allocate a new int list
	 *
	 * @return pointer to the allocated buffer, or 0 on failure
	 */
	pub fn axolotl_int_list_alloc() -> *mut ::IntList;

	/**
	 * Push a new value onto the end of the list
	 *
	 * @param list the list
	 * @param value the value to push
	 */
	pub fn axolotl_int_list_push_back(list: *mut ::IntList, value: types::c_int);

	/**
	 * Gets the size of the list.
	 *
	 * @param list the list
	 * @return the size of the list
	 */
	pub fn axolotl_int_list_size(list: *const ::IntList) -> types::c_uint;

	/**
	 * Gets the value of the element at a particular index in the list
	 *
	 * @param list the list
	 * @param index the index within the list
	 * @return the value
	 */
	pub fn axolotl_int_list_at(list: *mut ::IntList, index: types::c_uint) -> types::c_int;

	/**
	 * Free the int list
	 * @param list the list to free
	 */
	pub fn axolotl_int_list_free(list: *mut ::IntList);
}



#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct CryptoProvider {
	/**
	 * Callback for a secure random number generator.
	 * This function shall fill the provided buffer with random bytes.
	 *
	 * @param data pointer to the output buffer
	 * @param len size of the output buffer
	 * @return 0 on success, negative on failure
	 */
	pub random_func: Option<unsafe extern "C" fn(
					data: *mut u8, len: usize,
					user_data: *mut types::c_void
			) -> ::OkStatus>,
	
	/**
	 * Callback for an HMAC-SHA256 implementation.
	 * This function shall initialize an HMAC context with the provided key.
	 *
	 * @param hmac_context private HMAC context pointer
	 * @param key pointer to the key
	 * @param key_len length of the key
	 * @return 0 on success, negative on failure
	 */
	pub hmac_sha256_init_func: Option<unsafe extern "C" fn(
					hmac_context: *mut *mut types::c_void,
					key: *const u8, key_len: usize,
					user_data: *mut types::c_void
			) -> ::OkStatus>,
	
	/**
	 * Callback for an HMAC-SHA256 implementation.
	 * This function shall update the HMAC context with the provided data
	 *
	 * @param hmac_context private HMAC context pointer
	 * @param data pointer to the data
	 * @param data_len length of the data
	 * @return 0 on success, negative on failure
	 */
	pub hmac_sha256_update_func: Option<unsafe extern "C" fn(
					hmac_context: *mut types::c_void,
					data: *const u8, data_len: usize,
					user_data: *mut types::c_void
			) -> ::OkStatus>,
	
	/**
	 * Callback for an HMAC-SHA256 implementation.
	 * This function shall finalize an HMAC calculation and populate the output
	 * buffer with the result.
	 *
	 * @param hmac_context private HMAC context pointer
	 * @param output buffer to be allocated and populated with the result
	 * @return 0 on success, negative on failure
	 */
	pub hmac_sha256_final_func: Option<unsafe extern "C" fn(
					hmac_context: *mut types::c_void,
					output: *mut *mut ::Buffer,
					user_data: *mut types::c_void
			) -> ::OkStatus>,
	
	/**
	 * Callback for an HMAC-SHA256 implementation.
	 * This function shall free the private context allocated in
	 * hmac_sha256_init_func.
	 *
	 * @param hmac_context private HMAC context pointer
	 */
	pub hmac_sha256_cleanup_func: Option<unsafe extern "C" fn(
					hmac_context: *mut types::c_void,
					user_data: *mut types::c_void
			)>,
	
	/**
	 * Callback for a SHA512 message digest implementation.
	 * This function is currently only used by the fingerprint generator.
	 *
	 * @param output buffer to be allocated and populated with the ciphertext
	 * @param data pointer to the data
	 * @param data_len length of the data
	 * @return 0 on success, negative on failure
	 */
	pub sha512_digest_func: Option<unsafe extern "C" fn(
					output: *mut *mut ::Buffer,
					data: *const u8, data_len: usize,
					user_data: *mut types::c_void
			) -> ::OkStatus>,
	
	/**
	 * Callback for an AES encryption implementation.
	 *
	 * @param output buffer to be allocated and populated with the ciphertext
	 * @param cipher specific cipher variant to use, either AX_CIPHER_AES_CTR_NOPADDING or AX_CIPHER_AES_CBC_PKCS5
	 * @param key the encryption key
	 * @param key_len length of the encryption key
	 * @param iv the initialization vector
	 * @param iv_len length of the initialization vector
	 * @param plaintext the plaintext to encrypt
	 * @param plaintext_len length of the plaintext
	 * @return 0 on success, negative on failure
	 */
	pub encrypt_func: Option<unsafe extern "C" fn(
					output: *mut *mut ::Buffer,
					cipher: Cipher,
					key:       *const u8, key_len:       usize,
					iv:        *const u8, iv_len:        usize,
					plaintext: *const u8, plaintext_len: usize,
					user_data: *mut types::c_void
			) -> ::OkStatus>,

	/**
	 * Callback for an AES decryption implementation.
	 *
	 * @param output buffer to be allocated and populated with the plaintext
	 * @param cipher specific cipher variant to use, either AX_CIPHER_AES_CTR_NOPADDING or AX_CIPHER_AES_CBC_PKCS5
	 * @param key the encryption key
	 * @param key_len length of the encryption key
	 * @param iv the initialization vector
	 * @param iv_len length of the initialization vector
	 * @param ciphertext the ciphertext to decrypt
	 * @param ciphertext_len length of the ciphertext
	 * @return 0 on success, negative on failure
	 */
	pub decrypt_func: Option<unsafe extern "C" fn(
					output: *mut *mut ::Buffer,
					cipher: Cipher,
					key:        *const u8, key_len:        usize,
					iv:         *const u8, iv_len:         usize,
					ciphertext: *const u8, ciphertext_len: usize,
					user_data: *mut types::c_void
			) -> ::OkStatus>,

	/** User data pointer */
	pub user_data: *mut types::c_void,
}


#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SessionStore {
	/**
	 * Returns a copy of the serialized session record corresponding to the
	 * provided recipient ID + device ID tuple.
	 *
	 * @param record pointer to a freshly allocated buffer containing the
	 *     serialized session record. Unset if no record was found.
	 *     The axolotl library is responsible for freeing this buffer.
	 * @param address the address of the remote client
	 * @return 1 if the session was loaded, 0 if the session was not found, negative on failure
	 */
	pub load_session_func: Option<unsafe extern "C" fn(
					record: *mut *mut ::Buffer,
					address: *const Address,
					user_data: *mut types::c_void
			) -> ::ResponseStatus>,
	
	
	/**
	 * Returns all known devices with active sessions for a recipient
	 *
	 * @param pointer to an array that will be allocated and populated with the result
	 * @param name the name of the remote client
	 * @param name_len the length of the name
	 * @return size of the sessions array, or negative on failure
	 */
	pub get_sub_device_sessions_func: Option<unsafe extern "C" fn(
					sessions: *mut *mut ::IntList,
					name: *const types::c_char, name_len: usize,
					user_data: *mut types::c_void
			) -> ::ValueStatus>,
	
	
	/**
	 * Commit to storage the session record for a given
	 * recipient ID + device ID tuple.
	 *
	 * @param address the address of the remote client
	 * @param record pointer to a buffer containing the serialized session
	 *        record for the remote client
	 * @param record_len length of the serialized session record
	 * @return 0 on success, negative on failure
	 */
	pub store_session_func: Option<unsafe extern "C" fn(
					address: *const Address,
					record: *mut u8, record_len: usize,
					user_data: *mut types::c_void
			) -> ::OkStatus>,
	
	
	/**
	 * Determine whether there is a committed session record for a
	 * recipient ID + device ID tuple.
	 *
	 * @param address the address of the remote client
	 * @return 1 if a session record exists, 0 otherwise.
	 */
	pub contains_session_func: Option<unsafe extern "C" fn(
					address: *const Address,
					user_data: *mut types::c_void
			) -> ::ResponseStatus>,
	
	
	/**
	 * Remove a session record for a recipient ID + device ID tuple.
	 *
	 * @param address the address of the remote client
	 * @return 1 if a session was deleted, 0 if a session was not deleted, negative on error
	 */
	pub delete_session_func: Option<unsafe extern "C" fn(
					address: *const Address,
					user_data: *mut types::c_void
			) -> ::ResponseStatus>,
	
	
	/**
	 * Remove the session records corresponding to all devices of a recipient ID.
	 *
	 * @param name the name of the remote client
	 * @param name_len the length of the name
	 * @return the number of deleted sessions on success, negative on failure
	 */
	pub delete_all_sessions_func: Option<unsafe extern "C" fn(
					name: *const types::c_char, name_len: usize,
					user_data: *mut types::c_void
			) -> ::ValueStatus>,
	
	
	/**
	 * Function called to perform cleanup when the data store context is being
	 * destroyed.
	 */
	pub destroy_func: Option<unsafe extern "C" fn(user_data: *mut types::c_void)>,
	
	
	/** User data pointer */
	pub user_data: *mut types::c_void,
}


#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct PreKeyStore {
	/**
	 * Load a local serialized PreKey record.
	 *
	 * @param record pointer to a newly allocated buffer containing the record,
	 *     if found. Unset if no record was found.
	 *     The axolotl library is responsible for freeing this buffer.
	 * @param pre_key_id the ID of the local serialized PreKey record
	 * @retval AX_SUCCESS if the key was found
	 * @retval AX_ERR_INVALID_KEY_ID if the key could not be found
	 */
	pub load_pre_key: Option<unsafe extern "C" fn(
					record: *mut *mut ::Buffer,
					pre_key_id: u32,
					user_data:*mut types::c_void
			) -> ::OkStatus>,
	
	
	/**
	 * Store a local serialized PreKey record.
	 *
	 * @param pre_key_id the ID of the PreKey record to store.
	 * @param record pointer to a buffer containing the serialized record
	 * @param record_len length of the serialized record
	 * @return 0 on success, negative on failure
	 */
	pub store_pre_key: Option<unsafe extern "C" fn(
					pre_key_id: u32,
					record: *mut u8, record_len: usize,
					user_data: *mut types::c_void
			) -> ::OkStatus>,
	
	
	/**
	 * Determine whether there is a committed PreKey record matching the
	 * provided ID.
	 *
	 * @param pre_key_id A PreKey record ID.
	 * @return 1 if the store has a record for the PreKey ID, 0 otherwise
	 */
	pub contains_pre_key: Option<unsafe extern "C" fn(
					pre_key_id: u32,
					user_data: *mut types::c_void
			) -> ::ResponseStatus>,
	
	
	/**
	 * Delete a PreKey record from local storage.
	 *
	 * @param pre_key_id The ID of the PreKey record to remove.
	 * @return 0 on success, negative on failure
	 */
	pub remove_pre_key: Option<unsafe extern "C" fn(
					pre_key_id: u32,
					user_data: *mut types::c_void
			) -> ::OkStatus>,
	
	
	/**
	 * Function called to perform cleanup when the data store context is being
	 * destroyed.
	 */
	pub destroy_func: Option<unsafe extern "C" fn(user_data: *mut types::c_void)>,
	
	
	/** User data pointer */
	pub user_data: *mut types::c_void,
}


#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SignedPreKeyStore {
	/**
	 * Load a local serialized signed PreKey record.
	 *
	 * @param record pointer to a newly allocated buffer containing the record,
	 *     if found. Unset if no record was found.
	 *     The axolotl library is responsible for freeing this buffer.
	 * @param signed_pre_key_id the ID of the local signed PreKey record
	 * @retval AX_SUCCESS if the key was found
	 * @retval AX_ERR_INVALID_KEY_ID if the key could not be found
	 */
	pub load_signed_pre_key: Option<unsafe extern "C" fn(
					record: *mut *mut ::Buffer,
					signed_pre_key_id: u32,
					user_data: *mut types::c_void
			) -> ::OkStatus>,
	
	
	/**
	 * Store a local serialized signed PreKey record.
	 *
	 * @param signed_pre_key_id the ID of the signed PreKey record to store
	 * @param record pointer to a buffer containing the serialized record
	 * @param record_len length of the serialized record
	 * @return 0 on success, negative on failure
	 */
	pub store_signed_pre_key: Option<unsafe extern "C" fn(
					signed_pre_key_id: u32,
					record: *mut u8, record_len: usize,
					user_data: *mut types::c_void
			) -> ::OkStatus>,
	
	
	/**
	 * Determine whether there is a committed signed PreKey record matching
	 * the provided ID.
	 *
	 * @param signed_pre_key_id A signed PreKey record ID.
	 * @return 1 if the store has a record for the signed PreKey ID, 0 otherwise
	 */
	pub contains_signed_pre_key: Option<unsafe extern "C" fn(
					signed_pre_key_id: u32,
					user_data: *mut types::c_void
			) -> ::ResponseStatus>,
	
	
	/**
	 * Delete a SignedPreKeyRecord from local storage.
	 *
	 * @param signed_pre_key_id The ID of the signed PreKey record to remove.
	 * @return 0 on success, negative on failure
	 */
	pub remove_signed_pre_key: Option<unsafe extern "C" fn(
					signed_pre_key_id: u32,
					user_data: *mut types::c_void
			) -> ::OkStatus>,
	
	
	/**
	 * Function called to perform cleanup when the data store context is being
	 * destroyed.
	 */
	pub destroy_func: Option<unsafe extern "C" fn(user_data: *mut types::c_void)>,
	
	
	/** User data pointer */
	pub user_data: *mut types::c_void,
}


#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct IdentityKeyStore {
	/**
	 * Get the local client's identity key pair.
	 *
	 * @param public_data pointer to a newly allocated buffer containing the
	 *     public key, if found. Unset if no record was found.
	 *     The axolotl library is responsible for freeing this buffer.
	 * @param private_data pointer to a newly allocated buffer containing the
	 *     private key, if found. Unset if no record was found.
	 *     The axolotl library is responsible for freeing this buffer.
	 * @return 0 on success, negative on failure
	 */
	pub get_identity_key_pair: Option<unsafe extern "C" fn(
					public_data: *mut *mut ::Buffer,
					private_data: *mut *mut ::Buffer,
					user_data: *mut types::c_void
			) -> ::OkStatus>,
	
	
	/**
	 * Return the local client's registration ID.
	 *
	 * Clients should maintain a registration ID, a random number
	 * between 1 and 16380 that's generated once at install time.
	 *
	 * @param registration_id pointer to be set to the local client's
	 *     registration ID, if it was successfully retrieved.
	 * @return 0 on success, negative on failure
	 */
	pub get_local_registration_id: Option<unsafe extern "C" fn(
					user_data: *mut types::c_void,
					registration_id: *mut u32
			) -> ::OkStatus>,
	
	
	/**
	 * Save a remote client's identity key
	 * <p>
	 * Store a remote client's identity key as trusted.
	 * The value of key_data may be null. In this case remove the key data
	 * from the identity store, but retain any metadata that may be kept
	 * alongside it.
	 *
	 * @param name the name of the remote client
	 * @param name_len the length of the name
	 * @param key_data Pointer to the remote client's identity key, may be null
	 * @param key_len Length of the remote client's identity key
	 * @return 0 on success, negative on failure
	 */
	pub save_identity: Option<unsafe extern "C" fn(
					name: *const types::c_char, name_len: usize,
					key_data: *mut u8, key_len: usize,
					user_data: *mut types::c_void
			) -> ::OkStatus>,
	
	
	/**
	 * Verify a remote client's identity key.
	 *
	 * Determine whether a remote client's identity is trusted.  Convention is
	 * that the TextSecure protocol is 'trust on first use.'  This means that
	 * an identity key is considered 'trusted' if there is no entry for the recipient
	 * in the local store, or if it matches the saved key for a recipient in the local
	 * store.  Only if it mismatches an entry in the local store is it considered
	 * 'untrusted.'
	 *
	 * @param name the name of the remote client
	 * @param name_len the length of the name
	 * @param identityKey The identity key to verify.
	 * @param key_data Pointer to the identity key to verify
	 * @param key_len Length of the identity key to verify
	 * @return 1 if trusted, 0 if untrusted, negative on failure
	 */
	pub is_trusted_identity: Option<unsafe extern "C" fn(
					name: *const types::c_char, name_len: usize,
					key_data: *mut u8, key_len: usize,
					user_data: *mut types::c_void
			) -> ::ResponseStatus>,
	
	
	/**
	 * Function called to perform cleanup when the data store context is being
	 * destroyed.
	 */
	pub destroy_func: Option<unsafe extern "C" fn(user_data: *mut types::c_void)>,
	
	
	/** User data pointer */
	pub user_data: *mut types::c_void,
}


#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SenderKeyStore {
	/**
	 * Store a serialized sender key record for a given
	 * (groupId + senderId + deviceId) tuple.
	 *
	 * @param sender_key_name the (groupId + senderId + deviceId) tuple
	 * @param record pointer to a buffer containing the serialized record
	 * @param record_len length of the serialized record
	 * @return 0 on success, negative on failure
	 */
	pub store_sender_key: Option<unsafe extern "C" fn(
					sender_key_name: *const sender::KeyName,
					record: *mut u8, record_len: usize,
					user_data: *mut types::c_void
			) -> ::OkStatus>,
	
	
	/**
	 * Returns a copy of the sender key record corresponding to the
	 * (groupId + senderId + deviceId) tuple.
	 *
	 * @param record pointer to a newly allocated buffer containing the record,
	 *     if found. Unset if no record was found.
	 *     The axolotl library is responsible for freeing this buffer.
	 * @param sender_key_name the (groupId + senderId + deviceId) tuple
	 * @return 1 if the record was loaded, 0 if the record was not found, negative on failure
	 */
	pub load_sender_key: Option<unsafe extern "C" fn(
					record: *mut *mut ::Buffer,
					sender_key_name: *const sender::KeyName,
					user_data: *mut types::c_void
			) -> ::ResponseStatus>,
	
	
	/**
	 * Function called to perform cleanup when the data store context is being
	 * destroyed.
	 */
	pub destroy_func: Option<unsafe extern "C" fn(user_data: *mut types::c_void)>,
	
	
	/** User data pointer */
	pub user_data: *mut types::c_void,
}


extern "C" {
	/**
	 * Create a new instance of the global library context.
	 */
	pub fn axolotl_context_create(
			context:   *mut *mut ::Context,
			user_data: *mut types::c_void) -> ::OkStatus;
	
	
	/**
	 * Set the crypto provider to be used by the AXOLOTL library.
	 *
	 * @param crypto_provider Populated structure of crypto provider function
	 *     pointers. The contents of this structure are copied, so the caller
	 *     does not need to maintain its instance.
	 * @return 0 on success, negative on failure
	 */
	pub fn axolotl_context_set_crypto_provider(
			context:         *mut ::Context,
			crypto_provider: *const CryptoProvider) -> ::OkStatus;
	
	
	/**
	 * Set the locking functions to be used by the AXOLOTL library for
	 * synchronization.
	 *
	 * Note: These functions must allow recursive locking (e.g. PTHREAD_MUTEX_RECURSIVE)
	 *
	 * @param lock function to lock a mutex
	 * @param unlock function to unlock a mutex
	 * @return 0 on success, negative on failure
	 */
	pub fn axolotl_context_set_locking_functions(
			context: *mut ::Context,
			lock:    Option<unsafe extern "C" fn(user_data: *mut types::c_void)>,
			unlock:  Option<unsafe extern "C" fn(user_data: *mut types::c_void)>) -> ::OkStatus;
	
	
	/**
	 * Set the log function to be used by the AXOLOTL library for logging.
	 *
	 * @return 0 on success, negative on failure
	 */
	pub fn axolotl_context_set_log_function(
			context: *mut ::Context,
			log: Option<unsafe extern "C" fn(
					level:     LogLevel,
					message:   *const types::c_char, len: usize,
					user_data: *mut types::c_void)>
			) -> ::OkStatus;
	
	
	pub fn axolotl_context_destroy(context: *mut ::Context);
	
	
	/**
	 * Create a new instance of the AXOLOTL data store interface.
	 */
	pub fn axolotl_store_context_create(
			context: *mut *mut ::StoreContext,
			global_context: *mut ::Context) -> ::OkStatus;
	
	pub fn axolotl_store_context_set_session_store(
			context: *mut ::StoreContext,
			store:   *const SessionStore) -> ::OkStatus;
	
	pub fn axolotl_store_context_set_pre_key_store(
			context: *mut ::StoreContext,
			store:   *const PreKeyStore) -> ::OkStatus;
	
	pub fn axolotl_store_context_set_signed_pre_key_store(
			context: *mut ::StoreContext,
			store:   *const SignedPreKeyStore) -> ::OkStatus;
	
	pub fn axolotl_store_context_set_identity_key_store(
			context: *mut ::StoreContext,
			store:   *const IdentityKeyStore) -> ::OkStatus;
	
	pub fn axolotl_store_context_set_sender_key_store(
			context: *mut ::StoreContext,
			store:   *const SenderKeyStore) -> ::OkStatus;
	
	
	pub fn axolotl_store_context_destroy(context: *mut ::StoreContext);
	
	
	
	/*
	 * Interface to the session store.
	 * These functions will use the callbacks in the provided
	 * axolotl_store_context instance and operate in terms of higher level
	 * library data structures.
	 */
	
	pub fn axolotl_session_load_session(
			context: *mut ::StoreContext,
			record:  *mut *mut session::Record,
			address: *const Address) -> ::OkStatus;
	
	pub fn axolotl_session_get_sub_device_sessions(
			context:  *mut ::StoreContext,
			sessions: *mut *mut ::IntList,
			name:     *const types::c_char, name_len: usize) -> ::OkStatus;
	
	pub fn axolotl_session_store_session(
			context: *mut ::StoreContext,
			address: *const Address,
			record: *mut session::Record) -> ::OkStatus;
	
	pub fn axolotl_session_contains_session(
			context: *mut ::StoreContext,
			address: *const Address) -> ::ResponseStatus;
	
	pub fn axolotl_session_delete_session(
			context: *mut ::StoreContext,
			address: *const Address) -> ::ResponseStatus;
	
	pub fn axolotl_session_delete_all_sessions(
			context: *mut ::StoreContext,
			name:    *const types::c_char, name_len: usize) -> ::ValueStatus;
	
	
	
	/*
	 * Interface to the pre-key store.
	 * These functions will use the callbacks in the provided
	 * axolotl_store_context instance and operate in terms of higher level
	 * library data structures.
	 */
	
	pub fn axolotl_pre_key_load_key(
			context: *mut ::StoreContext,
			pre_key: *mut *mut session::PreKey,
			pre_key_id: u32) -> ::OkStatus;
	
	pub fn axolotl_pre_key_store_key(
			context: *mut ::StoreContext,
			pre_key: *mut session::PreKey) -> ::OkStatus;
	
	pub fn axolotl_pre_key_contains_key(
			context: *mut ::StoreContext,
			pre_key_id: u32) -> ::Comparision;
	
	pub fn axolotl_pre_key_remove_key(
			context: *mut ::StoreContext,
			pre_key_id: u32) -> ::ResponseStatus;
	
	
	
	/*
	 * Interface to the signed pre-key store.
	 * These functions will use the callbacks in the provided
	 * axolotl_store_context instance and operate in terms of higher level
	 * library data structures.
	 */
	
	pub fn axolotl_signed_pre_key_load_key(
			context: *mut ::StoreContext,
			pre_key: *mut *mut session::SignedPreKey,
			signed_pre_key_id: u32) -> ::OkStatus;
	
	pub fn axolotl_signed_pre_key_store_key(
			context: *mut ::StoreContext,
			pre_key: *mut session::SignedPreKey) -> ::OkStatus;
	
	pub fn axolotl_signed_pre_key_contains_key(
			context: *mut ::StoreContext,
			signed_pre_key_id: u32) -> ::Comparision;
	
	pub fn axolotl_signed_pre_key_remove_key(
			context: *mut ::StoreContext,
			signed_pre_key_id: u32) -> ::ResponseStatus;
	
	
	
	/*
	 * Interface to the identity key store.
	 * These functions will use the callbacks in the provided
	 * axolotl_store_context instance and operate in terms of higher level
	 * library data structures.
	 */
	
	pub fn axolotl_identity_get_key_pair(
			context: *mut ::StoreContext,
			key_pair: *mut *mut ratchet::IdentityKeyPair) -> ::OkStatus;
	
	pub fn axolotl_identity_get_local_registration_id(
			context: *mut ::StoreContext,
			registration_id: *mut u32) -> ::OkStatus;
	
	pub fn axolotl_identity_save_identity(
			context: *mut ::StoreContext,
			name: *const types::c_char, name_len: usize,
			identity_key: *mut curve::PublicKey) -> ::OkStatus;
	
	pub fn axolotl_identity_is_trusted_identity(
			context: *mut ::StoreContext,
			name: *const types::c_char, name_len: usize,
			identity_key: *mut curve::PublicKey) -> ::OkStatus;
}

