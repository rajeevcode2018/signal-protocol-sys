#[macro_use]
extern crate bitflags;

use std::os::raw as types;

pub mod axolotl;
pub mod curve;
pub mod fingerprint;
pub mod group;
pub mod hkdf;
pub mod keyhelper;
pub mod protocol;
pub mod ratchet;
pub mod sender;
pub mod session;


// Base library types
pub enum TypeBase   {}
pub enum Buffer     {}
pub enum BufferList {}
pub enum IntList    {}

/// Global context for the AXOLOTL library
pub enum Context {}

/// Context for the AXOLOTL data store implementation
pub enum StoreContext {}



/**
 * Comparision results as returned by most `*_compare(…)` functions
 */
#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Comparision {
	Greater = 1,
	Equal   = 0,
	Lower   = -1,
}

/**
 * Response status codes returned by many library functions
 */
#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Response {
	Yes = 1,
	No  = 0,
}
impl From<Response> for bool {
	fn from(response: Response) -> bool {
		match response as types::c_int {
			0 => false,
			_ => true
		}
	}
}



pub trait AsResult<T> {
	fn as_result(&self) -> Result<T, Error>;
}



/**
 * Returned by many library functions to indicate both whether a certain condition is met or not
 * and if there was an error while performing the operation
 */
#[repr(C, packed)]
#[derive(Clone, PartialEq, Eq, Copy)]
pub struct ResponseStatus {
	value: types::c_int
}
impl ResponseStatus {
	pub fn from_error(error: Error) -> Self {
		ResponseStatus {
			value: error as types::c_int
		}
	}
	
	pub fn from_response(response: Response) -> Self {
		ResponseStatus {
			value: response as types::c_int
		}
	}
	
	pub fn is_ok(&self) -> bool {
		self.value >= 0
	}
}

impl AsResult<bool> for ResponseStatus {
	fn as_result(&self) -> Result<bool, Error> {
		if self.is_ok() {
			match self.value {
				0 => Ok(false),
				_ => Ok(true)
			}
		} else {
			Err(Error::from_code(self.value))
		}
	}
}

impl std::fmt::Debug for ResponseStatus {
	fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self.as_result() {
			Ok(response) => write!(fmt, "ResponseStatus::Response({:?})", response),
			Err(error)   => write!(fmt, "ResponseStatus::Error({:?})",    error)
		}
	}
}


/**
 * Returned by many library function to indicate an amount on success or an error on failure
 */
#[repr(C, packed)]
#[derive(Clone, PartialEq, Eq, Copy)]
pub struct ValueStatus {
	value: types::c_int
}
impl ValueStatus {
	pub fn from_error(error: Error) -> Self {
		ValueStatus {
			value: error as types::c_int
		}
	}
	
	pub fn from_value(value: usize) -> Self {
		ValueStatus {
			value: value as types::c_int
		}
	}
	
	pub fn is_ok(&self) -> bool {
		self.value >= 0
	}
}

impl AsResult<usize> for ValueStatus {
	fn as_result(&self) -> Result<usize, Error> {
		if self.is_ok() {
			Ok(self.value as usize)
		} else {
			Err(Error::from_code(self.value))
		}
	}
}

impl std::fmt::Debug for ValueStatus {
	fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self.as_result() {
			Ok(value)  => write!(fmt, "ValueStatus::Value({:?})", value),
			Err(error) => write!(fmt, "ValueStatus::Error({:?})", error)
		}
	}
}


/**
 * Returned by many library function to indicate whether the operation succeed and how it failed
 */
#[repr(C, packed)]
#[derive(Clone, PartialEq, Eq, Copy)]
pub struct OkStatus {
	value: types::c_int
}
impl OkStatus {
	pub fn from_error(error: Error) -> Self {
		OkStatus {
			value: error as types::c_int
		}
	}
	
	pub fn from_success() -> Self {
		OkStatus {
			value: 0
		}
	}
	
	pub fn is_ok(&self) -> bool {
		self.value >= 0
	}
}

impl AsResult<()> for OkStatus {
	fn as_result(&self) -> Result<(), Error> {
		if self.is_ok() {
			Ok(())
		} else {
			Err(Error::from_code(self.value))
		}
	}
}

impl std::fmt::Debug for OkStatus {
	fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self.as_result() {
			Ok(_)      => write!(fmt, "OkStatus::Success"),
			Err(error) => write!(fmt, "OkStatus::Error({:?})", error)
		}
	}
}


/**
 * Error status codes returned by library functions
 */
#[repr(C)]
#[derive(Debug, Clone, PartialEq, Eq, Copy)]
pub enum Error {
	// Standard error codes with values that match errno.h equivalents
	
	/// Operation not permitted
	EPERM   = -1,
	/// No such file or directory
	ENOENT  = -2,
	/// No such process
	ESRCH   = -3,
	/// Interrupted system call
	EINTR   = -4,
	/// I/O Error
	EIO     = -5,
	/// No such device or address
	ENXIO   = -6,
	/// Argument list to long
	E2BIG   = -7,
	/// Exec format error
	ENOEXEC = -8,
	/// Bad file descriptor
	EBADF   = -9,
	/// No child processes
	ECHILD  = -10,
	/// Try again
	EAGAIN  = -11,
	/// Out of memory
	ENOMEM  = -12,
	/// Permission denied
	EACCES  = -13,
	/// Bad address
	EFAULT  = -14,
	/// Block device required
	ENOTBLK = -15,
	/// Device or resource busy
	EBUSY   = -16,
	/// File exists
	EEXIST  = -17,
	/// Cross-device link
	EXDEV   = -18,
	/// No such device
	ENODEV  = -19,
	/// Not a directory
	ENOTDIR = -20,
	/// Is a directory
	EISDIR  = -21,
	/// Invalid argument
	EINVAL  = -22,
	/// File table overflow
	ENFILE  = -23,
	/// Too many open files
	EMFILE  = -24,
	/// Not a teletyper
	ENOTTY  = -25,
	/// Text file busy
	ETXTBSY = -26,
	/// File too large
	EFBIG   = -27,
	/// No space left on device
	ENOSPEC = -28,
	/// Illegal seek
	ESPIPE  = -29,
	/// Read-only file system
	EROFS   = -30,
	/// Too many links
	EMLINK  = -31,
	/// Broken pipe
	EPIPE   = -32,
	/// Math argument out of domain of func
	EDOM    = -33,
	/// Math result not representable
	ERANGE  = -34,
	
	// Custom error codes for error conditions specific to the library
	
	Unknown           = -1000,
	DuplicateMessage  = -1001,
	InvalidKey        = -1002,
	InvalidKeyId      = -1003,
	InvalidMac        = -1004,
	InvalidMessage    = -1005,
	InvalidVersion    = -1006,
	LegacyMessage     = -1007,
	NoSession         = -1008,
	StaleKeyExchange  = -1009,
	UntrustedIdentity = -1010,
	InvalidProtoBuf   = -1100,
	FPVersionMismatch = -1200,
	FPIdentMismatch   = -1201,
	
	#[doc(hidden)]
	__Nonexhaustive = -9999,
}
impl Error {
	/**
	 * Convert the given AXOLOTL error code to an `Error` enum variant
	 *
	 * All unknown error codes will result in `Error::Unknown`.
	 */
	pub fn from_code(code: types::c_int) -> Self {
		match code {
			// Known error codes
			-34 ... -1 | -1201 ... -1000 => {
				// Yes, Rust's `std::num::FromPrimitve` has been ditched…
				unsafe { std::mem::transmute(code) }
			},
			
			// Unown error codes
			_ => {
				Error::Unknown
			}
		}
	}
	
	
	/**
	 * Convert the standard Rust I/O error to a library status code
	 *
	 * This will only work for `std::io::Error` instances that were created using the
	 * `from_raw_os_error` method and only if their errro code value is in the range `1 ... 34`.
	 */
	pub fn from_io_error(io_error: &std::io::Error) -> Self {
		if let Some(errno) = io_error.raw_os_error() {
			Self::from_code(-errno)
		} else {
			Error::Unknown
		}
	}
	
	
	/**
	 * Convert the given library error status to a standard Rust I/O error
	 *
	 * This will map error codes stored in the enum into "raw os error"-codes, while converting all
	 * other possible error states to string messages.
	 */
	pub fn to_io_error(&self) -> std::io::Error {
		match *self as i32 {
			errno @ -999 ... -1 =>
				std::io::Error::from_raw_os_error(-errno),
			
			_ =>
				std::io::Error::new(
					std::io::ErrorKind::Other,
					format!("signal_protocol_sys::Error::{:?}", self)
				)
		}
	}
}
impl std::fmt::Display for Error {
	fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
		self.to_io_error().fmt(fmt)
	}
}

/// Minimum negative error code value that this library may use.
/// When implementing library callback functions, using values
/// less than this constant will ensure that application-specific
/// errors can be distinguished from library errors.
pub const ERROR_MINIMUM: i32 = -9999;