use curve;
use hkdf;
use session;


// Ratchet types
pub const CIPHER_KEY_LENGTH: usize = 32;
pub const MAC_KEY_LENGTH:    usize = 32;
pub const IV_LENGTH:         usize = 16;

pub enum ChainKey {}
pub enum RootKey {}
pub enum IdentityKeyPair {}

#[repr(C)]
pub struct MessageKeys {
	cipher_key: [u8; CIPHER_KEY_LENGTH],
	mac_key:    [u8; MAC_KEY_LENGTH],
	iv:         [u8; IV_LENGTH],
	
	counter: u32,
}

pub enum SymmetricAxolotlParameters { }
pub enum AliceAxolotlParameters     { }
pub enum BobAxolotlParameters       { }


// ratchet.h //
extern "C" {
	pub fn ratchet_chain_key_create(
			chain_key: *mut *mut ChainKey,
			kdf: *mut hkdf::Context,
			key: *mut u8, key_len: usize,
			index: u32,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	
	pub fn ratchet_chain_key_get_key(
			chain_key: *const ChainKey,
			buffer: *mut *mut ::Buffer
		) -> ::OkStatus;
	
	
	pub fn ratchet_chain_key_get_index(
			chain_key: *const ChainKey
		) -> u32;
	
	
	pub fn ratchet_chain_key_get_message_keys(
			chain_key:    *mut ChainKey,
			message_keys: *mut MessageKeys
		) -> ::OkStatus;
	
	
	pub fn ratchet_chain_key_create_next(
			chain_key:      *const ChainKey,
			next_chain_key: *mut *mut ChainKey
		) -> ::OkStatus;
	
	
	pub fn ratchet_chain_key_destroy(
			chain_key: *mut ChainKey
		);
	
	
	pub fn ratchet_root_key_create(
			root_key: *mut *mut RootKey,
			kdf: *mut hkdf::Context,
			key: *const u8, key_len: usize,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	
	pub fn ratchet_root_key_create_chain(
			root_key:          *mut RootKey,
			new_root_key:      *mut *mut RootKey,
			new_chain_key:     *mut *mut ChainKey,
			their_ratchet_key: *mut curve::PublicKey,
			our_ratchet_key_private: *mut curve::PrivateKey
		) -> ::OkStatus;
	
	
	pub fn ratchet_root_key_get_key(
			root_key: *mut RootKey,
			buffer:   *mut *mut ::Buffer
		) -> ::OkStatus;
	
	
	pub fn ratchet_root_key_compare(
			key1: *const RootKey,
			key2: *const RootKey
		) -> ::Comparision;
	
	
	pub fn ratchet_root_key_destroy(
			root_key: *mut RootKey
		);
	
	
	pub fn ratchet_identity_key_pair_create(
			key_pair:    *mut *mut IdentityKeyPair,
			public_key:  *mut curve::PublicKey,
			private_key: *mut curve::PrivateKey
		) -> ::OkStatus;
	
	
	pub fn ratchet_identity_key_pair_serialize(
			buffer:   *mut *mut ::Buffer,
			key_pair: *const IdentityKeyPair
		) -> ::OkStatus;
	
	
	pub fn ratchet_identity_key_pair_deserialize(
			key_pair: *mut *mut IdentityKeyPair,
			data: *const u8, len: usize,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	
	pub fn ratchet_identity_key_pair_get_public(
			key_pair: *const IdentityKeyPair
		) -> *mut curve::PublicKey;
	
	
	pub fn ratchet_identity_key_pair_get_private(
			key_pair: *const IdentityKeyPair
		) -> *mut curve::PrivateKey;
	
	
	pub fn ratchet_identity_key_pair_destroy(
			key_pair: *const IdentityKeyPair
		);
	
	
	pub fn symmetric_axolotl_parameters_create(
			parameters: *mut *mut SymmetricAxolotlParameters,
			our_identity_key:   *mut IdentityKeyPair,
			our_base_key:       *mut curve::KeyPair,
			our_ratchet_key:    *mut curve::KeyPair,
			their_base_key:     *mut curve::PublicKey,
			their_ratchet_key:  *mut curve::PublicKey,
			their_identity_key: *mut curve::PublicKey
		) -> ::OkStatus;
	
	
	pub fn symmetric_axolotl_parameters_get_our_identity_key(
			parameters: *const SymmetricAxolotlParameters
		) -> *mut IdentityKeyPair;
	
	
	pub fn symmetric_axolotl_parameters_get_our_base_key(
			parameters: *const SymmetricAxolotlParameters
		) -> *mut curve::KeyPair;
	
	
	pub fn symmetric_axolotl_parameters_get_our_ratchet_key(
			parameters: *const SymmetricAxolotlParameters
		) -> *mut curve::KeyPair;
	
	
	pub fn symmetric_axolotl_parameters_get_their_base_key(
			parameters: *const SymmetricAxolotlParameters
		) -> *mut curve::PublicKey;
	
	
	pub fn symmetric_axolotl_parameters_get_their_ratchet_key(
			parameters: *const SymmetricAxolotlParameters
		) -> *mut curve::PublicKey;
	
	
	pub fn symmetric_axolotl_parameters_get_their_identity_key(
			parameters: *const SymmetricAxolotlParameters
		) -> *mut curve::PublicKey;
	
	
	pub fn symmetric_axolotl_parameters_destroy(
			parameters: *mut SymmetricAxolotlParameters
		);
	
	
	pub fn alice_axolotl_parameters_create(
			parameters: *mut *mut AliceAxolotlParameters,
			our_identity_key: *mut IdentityKeyPair,
			our_base_key:     *mut curve::KeyPair,
			their_identity_key:     *mut curve::PublicKey,
			their_signed_pre_key:   *mut curve::PublicKey,
			their_one_time_pre_key: *mut curve::PublicKey,
			their_ratchet_key:      *mut curve::PublicKey
		) -> ::OkStatus;
	
	
	pub fn alice_axolotl_parameters_destroy(
			parameters: *mut AliceAxolotlParameters
		);
	
	
	pub fn bob_axolotl_parameters_create(
			parameters: *mut *mut BobAxolotlParameters,
			our_identity_key:     *mut IdentityKeyPair,
			our_signed_pre_key:   *mut curve::KeyPair,
			our_one_time_pre_key: *mut curve::KeyPair,
			our_ratchet_key:      *mut curve::KeyPair,
			their_identity_key:   *mut curve::PublicKey,
			their_base_key:       *mut curve::PublicKey
		) -> ::OkStatus;
	
	
	pub fn bob_axolotl_parameters_destroy(
			parameters: *mut BobAxolotlParameters
		);
	
	
	pub fn ratcheting_session_symmetric_initialize(
			state: *mut session::State,
			parameters: *mut SymmetricAxolotlParameters,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	
	pub fn ratcheting_session_alice_initialize(
			state: *mut session::State,
			parameters:     *mut AliceAxolotlParameters,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	
	pub fn ratcheting_session_bob_initialize(
			state: *mut session::State,
			parameters: *mut BobAxolotlParameters,
			global_context: *mut ::Context
		) -> ::OkStatus;
}