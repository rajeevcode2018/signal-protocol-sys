use std::os::raw as types;

use protocol;
use sender;

// Group types
pub enum SessionBuilder {}
pub enum Cipher         {}


// group_cipher.h //
/*
 * The main entry point for Axolotl group encrypt/decrypt operations.
 *
 * Once a session has been established with group_session_builder and a
 * sender_key_distribution_message has been distributed to each member of
 * the group, this class can be used for all subsequent encrypt/decrypt
 * operations within that session (i.e. until group membership changes).
 */
extern "C" {
	/**
	 * Construct a group cipher for encrypt/decrypt operations.
	 *
	 * The store and global contexts must remain valid for the lifetime of the
	 * group cipher.
	 *
	 * When finished, free the returned instance by calling group_cipher_free().
	 *
	 * @param cipher set to a freshly allocated group cipher instance
	 * @param store the axolotl_store_context to store all state information in
	 * @param sender_key_id the sender that messages will be encrypted to or decrypted from
	 * @param global_context the global library context
	 * @return 0 on success, or negative on failure
	 */
	pub fn group_cipher_create(
			cipher: *mut *mut Cipher,
			store:  *mut ::StoreContext,
			sender_key_id: *const sender::KeyName,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	/**
	 * Set the optional user data pointer for the group cipher.
	 *
	 * This is to give callback functions a way of accessing app specific
	 * context information for this cipher.
	 */
	pub fn group_cipher_set_user_data(
			cipher: *mut Cipher,
			user_data: *mut types::c_void
		);
	
	/**
	 * Get the optional user data pointer for the group cipher.
	 *
	 * This is to give callback functions a way of accessing app specific
	 * context information for this cipher.
	 */
	pub fn group_cipher_get_user_data(
			cipher: *mut Cipher
		) -> *mut types::c_void;
	
	/**
	 * Set the callback function that is called during the decrypt process.
	 *
	 * The callback function is called from within group_cipher_decrypt() after
	 * decryption is complete but before the updated session state has been
	 * committed to the session store. If the callback function returns a
	 * negative value, then the decrypt function will immediately fail with
	 * an error.
	 *
	 * This a callback allows some implementations to store the committed plaintext
	 * to their local message store first, in case they are concerned with a crash
	 * or write error happening between the time the session state is updated but
	 * before they're able to successfully store the plaintext to disk.
	 *
	 * @param callback the callback function to set
	 * @param user_data user data pointer provided to the callback
	 */
	pub fn group_cipher_set_decryption_callback(
			cipher:   *mut Cipher,
			callback: Option<unsafe extern "C" fn(
				cipher:          *mut Cipher,
				plaintext:       *mut ::Buffer,
				decrypt_context: *mut types::c_void
			) -> ::OkStatus>
		);
	
	/**
	 * Encrypt a message.
	 *
	 * @param padded_message The plaintext message bytes, optionally padded to a constant multiple.
	 * @param padded_message_len The length of the data pointed to by padded_message
	 * @param encrypted_message Set to a ciphertext message encrypted to the group+sender+device tuple.
	 *
	 * @return AX_SUCCESS on success, negative on error
	 */
	pub fn group_cipher_encrypt(
			cipher:            *mut Cipher,
			padded_plaintext:  *const u8, padded_plaintext_len: usize,
			encrypted_message: *mut *mut protocol::CiphertextMessage
		) -> ::OkStatus;
	
	/**
	 * Decrypt a message.
	 *
	 * @param ciphertext The sender_key_message to decrypt.
	 * @param decrypt_context Optional context pointer associated with the
	 *   ciphertext, which is passed to the decryption callback function
	 * @param plaintext Set to a newly allocated buffer containing the plaintext.
	 *
	 * @retval AX_SUCCESS Success
	 * @retval AX_ERR_INVALID_MESSAGE if the input is not valid ciphertext.
	 * @retval AX_ERR_DUPLICATE_MESSAGE if the input is a message that has already been received.
	 * @retval AX_ERR_LEGACY_MESSAGE if the input is a message formatted by a protocol version that
	 *                               is no longer supported.
	 * @retval AX_ERR_NO_SESSION if there is no established session for this contact.
	 */
	pub fn group_cipher_decrypt(
			cipher:          *mut Cipher,
			ciphertext:      *mut sender::MessageKey,
			decrypt_context: *mut types::c_void,
			plaintext:       *mut *mut ::Buffer
		) -> ::OkStatus;
	
	pub fn group_cipher_free(
			cipher: *mut Cipher
		);
}


// group_session_builder.h
/*
 * Group session builder is responsible for setting up group sender key encrypted sessions.
 *
 * Once a session has been established, group_cipher can be used to
 * encrypt/decrypt messages in that session.
 * <p>
 * The built sessions are unidirectional: they can be used either for sending
 * or for receiving, but not both.
 *
 * Sessions are constructed per (groupId + senderId + deviceId) tuple.  Remote logical users
 * are identified by their senderId, and each logical recipientId can have multiple physical
 * devices.
 */
extern "C" {
	/**
	 * Constructs a group session builder.
	 *
	 * The store and global contexts must remain valid for the lifetime of the
	 * session builder.
	 *
	 * When finished, free the returned instance by calling group_session_builder_free().
	 *
	 * @param builder set to a freshly allocated group session builder instance
	 * @param store the axolotl_store_context to store all state information in
	 * @param global_context the global library context
	 * @return 0 on success, or negative on failure
	 */
	pub fn group_session_builder_create(
			builder: *mut *mut SessionBuilder,
			store:   *mut ::StoreContext,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	/**
	 * Construct a group session for receiving messages from senderKeyName.
	 *
	 * @param sender_key_name the (groupId, senderId, deviceId) tuple associated
	 *     with the sender_key_distribution_message
	 * @param distribution_message a received sender_key_distribution_message
	 * @return 0 on success, or negative on failure
	 */
	pub fn group_session_builder_process_session(
			builder: *mut SessionBuilder,
			sender_key_name:      *const sender::KeyName,
			distribution_message: *mut protocol::SenderKeyDistributionMessage
		) -> ::OkStatus;
	
	/**
	 * Construct a group session for sending messages.
	 *
	 * @param distribution_message a distribution message to be allocated and populated
	 * @param sender_key_name the (groupId, senderId, deviceId) tuple. In this
	 *     case, the sender should be the caller
	 * @return 0 on success, or negative on failure
	 */
	pub fn group_session_builder_create_session(
			builder: *mut SessionBuilder,
			distribution_message: *mut *mut protocol::SenderKeyDistributionMessage,
			sender_key_name:      *const sender::KeyName
		) -> ::OkStatus;
	
	pub fn group_session_builder_free(
			builder: *mut SessionBuilder
		);
}