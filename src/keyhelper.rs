use std::os::raw as types;

use curve;
use ratchet;
use session;



// Key helper types
pub enum PreKeyListNode {}


// key_helper.h //
extern "C" {
	/**
	 * Generate an identity key pair.  Clients should only do this once,
	 * at install time.
	 *
	 * @param key_pair the generated identity key pair
	 * @return 0 on success, or negative on failure
	 */
	pub fn axolotl_key_helper_generate_identity_key_pair(
			key_pair: *mut *mut ratchet::IdentityKeyPair,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	/**
	 * Generate a registration ID.  Clients should only do this once,
	 * at install time.
	 *
	 * @param registration_id set to the generated registration ID
	 * @param extendedRange By default (0), the generated registration
	 *                      ID is sized to require the minimal possible protobuf
	 *                      encoding overhead. Specify true (1) if the caller needs
	 *                      the full range of MAX_INT at the cost of slightly
	 *                      higher encoding overhead.
	 * @return 0 on success, or negative on failure
	 */
	pub fn axolotl_key_helper_generate_registration_id(
			registration_id: *mut u32,
			extended_range:  ::Response,
			global_context:  *mut ::Context
		) -> ::OkStatus;
	
	/**
	 * Generate a random number bounded by the provided maximum
	 *
	 * @param value set to the next random number
	 * @param max the maximum value of the random number
	 * @return 0 on success, or negative on failure
	 */
	pub fn axolotl_key_helper_get_random_sequence(
			value: *mut types::c_int,
			max:   types::c_int,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	/**
	 * Generate a list of PreKeys.  Clients should do this at install time, and
	 * subsequently any time the list of PreKeys stored on the server runs low.
	 *
	 * Pre key IDs are shorts, so they will eventually be repeated.  Clients should
	 * store pre keys in a circular buffer, so that they are repeated as infrequently
	 * as possible.
	 *
	 * When finished with this list, the caller should free it by calling
	 * axolotl_key_helper_key_list_free().
	 *
	 * @param head pointer to the head of the key list
	 * @param start the starting pre key ID, inclusive.
	 * @param count the number of pre keys to generate.
	 * @return 0 on success, or negative on failure
	 */
	pub fn axolotl_key_helper_generate_pre_keys(
			head:  *mut *mut PreKeyListNode,
			start: types::c_uint,
			count: types::c_uint,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	/**
	 * Get the pre key element for the current node in the key list.
	 *
	 * @param current list node
	 * @return pre key element
	 */
	pub fn axolotl_key_helper_key_list_element(
			node: *const PreKeyListNode
		) -> *mut session::PreKey;
	
	/**
	 * Get the next element in the key list.
	 *
	 * @param current list node
	 * @return next list node, or 0 if at the end of the list
	 */
	pub fn axolotl_key_helper_key_list_next(
			node: *const PreKeyListNode
		) -> *mut PreKeyListNode;
	
	/**
	 * Free the key list.
	 *
	 * @param head pointer to the head of the list to free
	 */
	pub fn axolotl_key_helper_key_list_free(
			head: *mut PreKeyListNode
		);
	
	/**
	 * Generate the last resort pre key.  Clients should do this only once, at
	 * install time, and durably store it for the length of the install.
	 *
	 * @param pre_key set to the generated pre key
	 * @return 0 on success, or negative on failure
	 */
	pub fn axolotl_key_helper_generate_last_resort_pre_key(
			pre_key: *mut *mut session::PreKey,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	/**
	 * Generate a signed pre key
	 *
	 * @param signed_pre_key set to the generated pre key
	 * @param identity_key_pair the local client's identity key pair.
	 * @param signed_pre_key_id the pre key ID to assign the generated signed pre key
	 * @param timestamp the current time in milliseconds since the UNIX epoch
	 *
	 * @return 0 on success, or negative on failure
	 */
	pub fn axolotl_key_helper_generate_signed_pre_key(
			signed_pre_key:    *mut *mut session::SignedPreKey,
			identity_key_pair: *const ratchet::IdentityKeyPair,
			signed_pre_key_id: u32,
			timestamp:         u64,
			global_context:    *mut ::Context
		) -> ::OkStatus;
	
	/*
	 * Generate a sender signing key pair
	 *
	 * @param key_pair the generated key pair
	 * @return 0 on success, or negative on failure
	 */
	pub fn axolotl_key_helper_generate_sender_signing_key(
			key_pair:       *mut *mut curve::KeyPair,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	/*
	 * Generate a sender key
	 *
	 * @param key_buffer buffer to be allocated and populated with the result
	 * @return 0 on success, or negative on failure
	 */
	pub fn axolotl_key_helper_generate_sender_key(
			key_buffer:     *mut *mut ::Buffer,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	/*
	 * Generate a sender key ID
	 *
	 * @param key_id assigned to the generated ID
	 * @return 0 on success, or negative on failure
	 */
	pub fn axolotl_key_helper_generate_sender_key_id(
			key_id:         *mut u32,
			global_context: *mut ::Context
		) -> ::OkStatus;
}